import Config

config :logger,
  handle_otp_reports: false,
  console: [
    format: "$date $time $message\n"
  ]
