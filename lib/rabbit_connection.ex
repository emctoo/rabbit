defmodule Rabbit.RmqConnction do
  use GenServer
  require Logger

  alias AMQP.Connection

  @url "amqp://guest:guest@127.0.0.1:5672/test"
  @reconnect_interval 3_000

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_opts) do
    Logger.debug("creating new rabbitmq connection ...")
    send(self(), :connect)

    {:ok, nil}
  end

  ### client apis

  def current do
    case GenServer.call(__MODULE__, :current) do
      nil -> {:error, :not_connected}
      conn -> {:ok, conn}
    end
  end

  ### server callbacks

  def handle_call(:current, _from, conn), do: {:reply, conn, conn}

  def handle_info(:connect, _state) do
    case AMQP.Connection.open(@url) do
      {:ok, %{pid: pid} = conn} ->
        Logger.info("connection established.")
        Process.monitor(pid)
        {:noreply, conn}

      {:error, _} ->
        Logger.error("fail to connect #{@url}")
        Process.send_after(self(), :connect, @reconnect_interval)
        {:noreply, nil}
    end
  end

  def handle_info({:DOWN, _, :process, _pid, reason}, _) do
    {:stop, {:connection_lost, reason}, nil}
  end
end
