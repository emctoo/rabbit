defmodule Rabbit.Setup do
  use AMQP

  @topic_exchange "test_topic_exchange"
  # @exchange "test_exchange"
  @queue "test_queue"
  @queue_error "#{@queue}_error"

  def init(_opts \\ []) do
    url = "amqp://guest:guest@localhost/test"
    {:ok, conn} = Connection.open(url)
    {:ok, chan} = Channel.open(conn)
    setup_queue(chan)

    Channel.close(chan)
    Connection.close(conn)
  end

  defp setup_queue(chan) do
    {:ok, _} = Queue.declare(chan, @queue_error, durable: true)

    # Messages that cannot be delivered to any consumer in the main queue will be routed to the error queue
    options = [
      durable: true,
      arguments: [
        {"x-dead-letter-exchange", :longstr, ""},
        {"x-dead-letter-routing-key", :longstr, @queue_error}
      ]
    ]

    {:ok, _} = Queue.declare(chan, @queue, options)

    :ok = Exchange.declare(chan, @topic_exchange, :topic, durable: true)
    :ok = Queue.bind(chan, @queue, @topic_exchange)
  end
end
