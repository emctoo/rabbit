defmodule Rabbit.Consumer do
  use GenServer
  use AMQP
  require Logger

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @queue "test_queue"

  def init(_opts) do
    Rabbit.Setup.init()

    url = "amqp://guest:guest@localhost/test"
    {:ok, conn} = Connection.open(url)
    {:ok, chan} = Channel.open(conn)

    # Limit unacknowledged messages to 7
    :ok = Basic.qos(chan, prefetch_count: 7)

    # register the GenServer process as a consumer, 仔细参阅文档
    {:ok, _consumer_tag} = Basic.consume(chan, @queue)

    {:ok, chan}
  end

  # confirmation sent by the broker after registering this process as a consumer
  def handle_info({:basic_consume_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  # sent by the broker when the consumer is unexpectedly cancelled (such as after a queue deletion)
  def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, chan) do
    {:stop, :normal, chan}
  end

  # confirmation sent by the broker to the consumer process after a Basic.cancel
  def handle_info({:basic_cancel_ok, %{consumer_tag: _consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info(
        {:basic_deliver, payload, %{delivery_tag: tag, redelivered: redelivered}} = params,
        chan
      ) do
    Logger.info("params: #{inspect(params, pretty: true)}")
    consume(chan, tag, redelivered, payload)
    {:noreply, chan}
  end

  defp consume(channel, deliver_tag, redelivered, payload) do
    Logger.info("in consume, redelivered: #{redelivered}, payload: #{String.length(payload)}")
    AMQP.Basic.ack(channel, deliver_tag)
  end
end
