defmodule Rabbit.Producer do
  use GenServer
  use AMQP
  require Logger

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @exchange "test_topic_exchange"

  def init(_opts) do
    Rabbit.Setup.init()

    url = "amqp://guest:guest@localhost/test"
    {:ok, conn} = Connection.open(url)
    {:ok, channel} = Channel.open(conn)

    {:ok, channel}
  end

  def handle_cast(:random, channel) do
    for _ <- 1..10 do
      payload = random_string()
      routing_key = Enum.random(["recsys_users.hello", "jsonb_profiles.world"])
      :ok = Basic.publish(channel, @exchange, routing_key, payload)
      Logger.info("message published")

      :timer.sleep(1_000)
    end

    {:noreply, channel}
  end

  def handle_info(args, channel) do
    Logger.info("args: #{inspect(args, pretty: true)}")
    {:noreply, channel}
  end

  def random_string() do
    now = DateTime.utc_now() |> DateTime.to_iso8601(:extended)
    length = :rand.uniform(100)
    random_part = :crypto.strong_rand_bytes(length) |> Base.encode16() |> binary_part(0, length)
    "#{now} #{random_part}"
  end
end
